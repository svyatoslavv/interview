import products from '../api/products.json'

/** True = 65%, False = 35% */
const rejectByChance = () => {
  return Math.random() <= 0.35
}

/** Emulate get request */
export const getProducts = (params) =>
  new Promise((resolve, reject) => {
    if (rejectByChance()) {
      // eslint-disable-next-line prefer-promise-reject-errors
      return resolve({
        error: 'Server error',
        success: false
      })
    }

    // filtering of array
    const count = params.itemsPerPage
    const page = params.page
    const sortBy = params.sortBy
    const sortUp = params.sortUp

    let result = products.slice()

    if (sortBy) {
      result.sort((a, b) => {
        if (sortUp) return a[sortBy] > b[sortBy] ? 1 : -1
        else return a[sortBy] < b[sortBy] ? 1 : -1
      })
    }

    if (count) {
      const begin = ((page || 1) - 1) * count
      const end = (page || 1) * count
      result = result.slice(begin, end)
    }

    const delay = parseInt(Math.random() * 1000)
    const res = {
      success: true,
      results: result,
      count: products.length
    }
    setTimeout(() => {
      resolve(res)
    }, delay)
  })

/** Emulate delete request */
export const deleteProducts = () =>
  new Promise((resolve, reject) => {
    if (rejectByChance()) {
      // eslint-disable-next-line prefer-promise-reject-errors
      return resolve({
        success: false,
        error: 'Server error'
      })
    }
    const delay = parseInt(Math.random() * 1000)
    setTimeout(() => {
      resolve({ message: 'deleted', success: true })
    }, delay)
  })
