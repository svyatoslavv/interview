import Vue from 'vue'

import STable from '~/components/simple/Table/index'
import SAutocomplete from '~/components/simple/autocomplete'
import SConfirm from '~/components/simple/confirm'
import SAlert from '~/components/simple/alert'

// Объявление глобальных компонентов
Vue.component('STable', STable)
Vue.component('SAutocompcdlete', SAutocomplete)
Vue.component('SConfirm', SConfirm)
Vue.component('SAlert', SAlert)
