import { getProducts, deleteProducts } from '~/mixins/request'
export const state = () => ({
  count: 0, // count of items
  items: []
})

export const mutations = {
  setItems: (state, params) => (state.items = params),
  setCount: (state, params) => (state.count = params)
}

export const actions = {
  // getting of items
  async getItems(ctx, params) {
    const res = await getProducts(params)

    // if error then return error
    // if success - commit items and count
    if (!res.success) return res
    else {
      ctx.commit('setItems', res.results)
      ctx.commit('setCount', res.count)
    }
  },

  /** Deleting of items */
  async deleteProducts(ctx, params) {
    const res = await deleteProducts(params)
    console.log(res)
    return res
  }
}
